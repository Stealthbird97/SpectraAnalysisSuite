import numpy as np
from lmfit.models import LorentzianModel, SplitLorentzianModel, SineModel, ConstantModel
import pandas as pd
import scipy as scipy
import matplotlib as matplotlib
import matplotlib.pyplot as plt
from natsort import natsorted
import ntpath
import itertools
import glob
import re
import regexs as regexs


def filebrowser(dir=".", ext=".csv"):
    "Returns files with an extension"
    return [f for f in glob.glob(f"{dir}\*{ext}", recursive=True)]


def groupfiles(filelist, grouplen):
    "Sorts file list into group of files of the same device"
    groups = [list(g) for _, g in itertools.groupby(natsorted(filelist, reverse=True), lambda x: x[0:grouplen])]
    return groups


def preparefiles(folder, grouplength, filetype='.csv'):
    "Returns a list of files in natural order, and a list of groups of files."
    files = filebrowser(folder, filetype)
    files = natsorted(files, reverse=True)
    groups = groupfiles(files, grouplength)
    return files, groups

def parse_filenames(filename):
    "Parses filenames into a dict"
    ntpath.basename("a/b/c")
    head, tail = ntpath.split(filename)
    filename = tail
    regex = regexs.S45862()
    match = re.match(regex, filename, re.VERBOSE)

    if match:
        capture_dict = match.groupdict()
        print(capture_dict)

    else:
        print("No match found.")
        print(filename)

    return capture_dict

def import_spectra(file):
    "Imports a single spectra file"
    data = pd.read_csv(file, names=["wavelength","Slice","RAWcounts","Corrected","c/s"],skiprows=4)
    return data


def import_spectra_group(groups, grouplen):
    "Imports groups of spectra into a dataframe containing the spectra and parsed data"
    df = pd.DataFrame(columns=['group','device', 'power', 'Spectra'])
    for _, group in enumerate(groups):
        groupname = group[0][:grouplen]
        for _, spectra in enumerate(group):
            parsed = parse_filenames(spectra)
            keys = list(list(parsed.keys())) + ["Spectra"] + ["group"]
            data = import_spectra(spectra)
            dataf = [*parsed.values(), data]
            dataframe = pd.DataFrame([[*parsed.values(), data, groupname]], columns=keys)
            df = pd.concat([df,dataframe])
    groupnames = df.groupby(["group"])
    return df, groupnames.groups

def plot_single_spectra(x,y,title,xaxis=r"Wavelength (nm)",yaxis=r"Counts/s"):
    "Plots a single spectra and returns a figure and axes"
    fig, ax = plt.subplots()
    ax.set_xlabel(xaxis)
    ax.set_ylabel(yaxis)
    ax.set_title(title)
    ax.plot(x,y)
    return fig, ax

def plot_group_spectra(group,labels,title,xaxis=r"Wavelength (nm)",yaxis=r"Counts/s"):
    "Plots a group of spectra and returns a figure and axes"
    fig, ax = plt.subplots()
    ax.set_xlabel(xaxis)
    ax.set_ylabel(yaxis)
    ax.set_title(title)
    for n, spectra in enumerate(group["Spectra"]):
        label = labels.iat[n]
        ax.plot(spectra["wavelength"],spectra["c/s"], label=f"{label}uW")
    ax.legend()
    return fig, ax

def fit_peak(x,y,center1, center2):
    model = LorentzianModel(prefix="l1_") + LorentzianModel(prefix="l2_") + ConstantModel()
    model.set_param_hint('l1_center', value=center1)
    model.set_param_hint('l2_center', value=center2)
    params = model.make_params()
    result = model.fit(y,params, x=x)
    return result

def integratespectra(x,y):
    inte = scipy.integrate.simpson(y,x)
    return inte

processed = pd.DataFrame()
grouplen = 40
files, groups = preparefiles("spectra", grouplen)
df, groupnames = import_spectra_group(groups, grouplen)
for n, group in enumerate(groupnames.items()):
    groupdata = df.loc[df['group'] == group[0]]
    labels = groupdata['power']
    data = groupdata.iloc[0]
    """title = f"{data['sample']}_{data['grid']}_{data['subgrid']}_{data['device']}"
    fig, ax = plot_group_spectra(groupdata, labels, title)
    plt.show()
    plt.close()"""

    for n, spectra in enumerate(groupdata["Spectra"]):
        data = groupdata.iloc[n]
        title = f"{data['sample']}_{data['grid']}_{data['subgrid']}_{data['device']}_{data['power']}uW"
        #fig,ax = plot_single_spectra(spectra["wavelength"],spectra["c/s"],title)
        integrate = integratespectra(spectra["wavelength"],spectra["c/s"])
        print(integrate)
        dataframe = pd.DataFrame([[data['sample'],data['grid'],data['subgrid'],data['device'],data['power'],integrate]], columns=["sample","grid","subgrid","device","power","integrated"])
        processed = pd.concat([processed, dataframe])
        #result = fit_peak(spectra["wavelength"],spectra["c/s"], 906.770417,907.405689)
        #ax.plot(spectra["wavelength"], result.best_fit)
        #print(result.fit_report())
        #fig.savefig(f"{title}.png")
        #plt.show()
        print("Finished")
processed.to_csv("Processed.csv")
print("Final Finish")
"""
data = df.iloc[0]
title = f"{data['sample']}_{data['grid']}_{data['subgrid']}_{data['device']}"
labels = df['power']
fig,ax = plot_group_spectra(df,labels,title)
plt.show()"""


