def S45862():
    regex = r"""
        (?P<sample>\w+)_                         # sample name
        (?P<grid>insideDring|outsideDring|insidering|outsidering)_                 # grid
        (?:(?P<subgrid>BR|noBR)_)?          # optional subgrid
        (?P<device>[A-Za-z]*\d+)_                # device
        (?:(?P<integration>\d+)s_)?              # optional integration time
        (?:(?P<acquisitions>\d+)acq_)?           # optional number of acquisitions
        (?:(?P<power>\d+)uW_)?                   # optional power
        (?:(?P<calibration>uncalibrated)_)?      # optional power calibration
        (?P<laser>\d+nm[A-Za-z]*)_               # laser
        (?:(?P<grating>1200Blz|600Blz)_)?        # optional grating
        (?P<filtered>[\d-]+FEL)_                 # filtered used
        (?:(?P<temperature>\d+K)_)?              # optional temperature
        \d+\.csv_Corrected\.csv                       # file extension
    """
    return regex
def S45349():
    regex = r"""
            (?P<sample>\w+)_                         # sample name
            (?P<grid>R\d+C\d+)_                      # grid
            (?P<subgrid>[A-Za-z]\d+)_                # subgrid
            (?P<device>QD\d+)_                       # device
            (?:(?P<integration>\d+)s_)?              # optional integration time
            (?:(?P<acquisitions>\d+)acq_)?           # optional number of acquisitions
            (?:(?P<power>\d+)uW_)?                   # optional power
            (?:(?P<calibration>uncalibrated)_)?      # optional power calibration
            (?P<laser>\d+nm)_                        # laser
            (?:(?P<grating>1200Blz|600Blz)_)?        # optional grating
            (?P<filtered>[\d-]+FEL|FES)_             # filters used
            (?:(?P<temperature>\d+)K_)?              # optional temperature
            \d+\.csv_Corrected\.csv                  # file extension
        """
    return regex
